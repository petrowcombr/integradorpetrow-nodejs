var http = require("https");

var locale = "pt"; // informe o seu locale (pt / es / en)
var login = "everton"; // informe o seu login
var token = "7c33b3eb16d599bd53b4f0a404e77b4c"; // informe a sua senha

var options = {
    "method": "POST",
    "hostname": "www.petrow.com.br",
    "port": null,
    "path": "/ws/ConversionTo20/?locale=" + locale + "&login=" + login + "&token=" + token + "&kind=E&temperature=23.7&density=0.8888&volume=1000.0",
    "headers": {
        "cache-control": "no-cache",
        "user-agent": "IntegradorPetroW-NODEJS"
    }
};

var req = http.request(options, function (res) {
    var chunks = [];

    res.on("data", function (chunk) {
        chunks.push(chunk);
    });

    res.on("end", function () {
        var body = Buffer.concat(chunks);
        
        console.log(body.toString());
    });
});

req.end();